package controlador;
import java.util.ArrayList;

import modelo.Grafo;
import modelo.Vertice;

public class AlgoritmoDePrim {
	private static double [][] matrizDePesos;
	private static int numeroDeVertices;
	private static ArrayList<Vertice> vertices;
	private static Grafo grafoOriginal;
	
	public static Grafo generarAGM(Grafo grafo){
		numeroDeVertices=grafo.getCantVertices();
		matrizDePesos=grafo.getMatrizDePesos();
		vertices=grafo.getVertices();
		grafoOriginal = grafo;
		
		Grafo grafoAGM = new Grafo();
		
		for (Vertice vertice: grafoOriginal.getVertices()) {
			grafoAGM.agregarVertice(vertice);
		}
		grafoAGM.vaciarListasVecinos();

		double menor;
		int verticeAUX; //Se utiliza para encontrar el vertice mas cercano y de menor peso
		double[] coste= new double[numeroDeVertices];
		int[] masCerca= new int[numeroDeVertices];
		boolean[] verticesVisitados = new boolean[numeroDeVertices];
		
		for(int i=0; i<numeroDeVertices ; i++)
			verticesVisitados[i]=false;
			verticesVisitados[0]=true;//se parte del vertice 0
		
		
		for (int i = 1 ; i < numeroDeVertices; i++){
			coste[i]= matrizDePesos[0][i];
			masCerca[i]=0;
		}
		for (int i=1; i<numeroDeVertices; i++){
			menor = coste[1];
			verticeAUX=1;
			for( int j=2; j<numeroDeVertices; j++)
			if(coste[j] < menor){
				menor=coste[j];
				verticeAUX=j;
			}
			
			grafoAGM.establecerComoVecinos(vertices.get(masCerca[verticeAUX]), vertices.get(verticeAUX));
			verticesVisitados[verticeAUX] = true;
			coste[verticeAUX] = Double.MAX_VALUE;//se le da el mayor valor posible
			for (int j=1; j<numeroDeVertices; j++)
				if ((matrizDePesos[verticeAUX][j] < coste[j])&& !verticesVisitados[j]){
					coste[j] = matrizDePesos[verticeAUX][j];
					masCerca[j]=verticeAUX;
				}
		}
		return grafoAGM;
	}
}
