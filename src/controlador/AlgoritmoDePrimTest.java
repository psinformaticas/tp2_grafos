package controlador;

import static org.junit.Assert.*;

import modelo.Coordenada;
import modelo.Grafo;
import modelo.Vertice;

import org.junit.Before;
import org.junit.Test;

public class AlgoritmoDePrimTest {
	Grafo grafoCompleto;
	Grafo grafoAGM;
	
	@Before
	public void inicializar(){
		grafoCompleto=new Grafo();
		grafoAGM=new Grafo();
		Coordenada c1=new Coordenada(-34.52133782929332,-58.70068073272705);
		Coordenada c2=new Coordenada(-34.520772089706036,-58.702311515808105);
		Coordenada c3=new Coordenada(-34.52126711205503,-58.70325565338135);
		Coordenada c4=new Coordenada(-34.52186820666683,-58.70265483856201);
		
		Vertice v1= new Vertice(1,c1);
		Vertice v2= new Vertice(2,c2);
		Vertice v3= new Vertice(3,c3);
		Vertice v4= new Vertice(4,c4);
		
		Vertice v1AGM= new Vertice(1,c1);
		Vertice v2AGM= new Vertice(2,c2);
		Vertice v3AGM= new Vertice(3,c3);
		Vertice v4AGM= new Vertice(4,c4);
		
		grafoCompleto.agregarVertice(v1);
		grafoCompleto.agregarVertice(v2);
		grafoCompleto.agregarVertice(v3);
		grafoCompleto.agregarVertice(v4);
		
		grafoCompleto.establecerComoVecinos(v1, v2);
		grafoCompleto.establecerComoVecinos(v1, v3);
		grafoCompleto.establecerComoVecinos(v1, v4);
		grafoCompleto.establecerComoVecinos(v2, v1);
		grafoCompleto.establecerComoVecinos(v2, v3);
		grafoCompleto.establecerComoVecinos(v2, v4);
		grafoCompleto.establecerComoVecinos(v3, v1);
		grafoCompleto.establecerComoVecinos(v3, v2);
		grafoCompleto.establecerComoVecinos(v3, v4);
		grafoCompleto.establecerComoVecinos(v4, v1);
		grafoCompleto.establecerComoVecinos(v4, v2);
		grafoCompleto.establecerComoVecinos(v4, v3);
		
		grafoAGM.agregarVertice(v1AGM);
		grafoAGM.agregarVertice(v2AGM);
		grafoAGM.agregarVertice(v3AGM);
		grafoAGM.agregarVertice(v4AGM);
		
		grafoAGM.establecerComoVecinos(v1AGM, v2AGM);
		grafoAGM.establecerComoVecinos(v2AGM, v1AGM);
		grafoAGM.establecerComoVecinos(v2AGM, v3AGM);
		grafoAGM.establecerComoVecinos(v3AGM, v2AGM);
		grafoAGM.establecerComoVecinos(v3AGM, v4AGM);
		grafoAGM.establecerComoVecinos(v4AGM, v3AGM);
		
	}
	
	@Test
	public void generarAGMTest(){
		assertTrue((AlgoritmoDePrim.generarAGM(grafoCompleto)).equals(grafoAGM));
	}
}
