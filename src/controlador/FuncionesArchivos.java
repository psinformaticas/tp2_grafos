package controlador;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import modelo.Coordenada;
import modelo.EnumFileChooser;
import modelo.Grafo;
import modelo.Vertice;

public class FuncionesArchivos {
	
	public static void guardarGrafoATxt(ArrayList<Vertice> arrayVertices, String ruta) {
		try 
		 {
		 FileOutputStream fos = new FileOutputStream(ruta);
		 OutputStreamWriter out = new OutputStreamWriter(fos);
		for (Vertice vertice: arrayVertices) {
			out.write(vertice.toString()+"\r\n");
			
		} 
		 out.close();
		 }
		 catch(Exception e) {}
		
	}

	public static ArrayList<Coordenada> leerCoordDesdeTxt(String ruta) {
		ArrayList<Coordenada> coords = new ArrayList<>();
		
		try {
			FileInputStream fis = new FileInputStream(ruta);
			Scanner scanner = new Scanner(fis);
			
			while (scanner.hasNext()) {
				String linea = scanner.nextLine();
				linea = linea.startsWith(" ") ? linea.substring(1) : linea;
				
				String arrAux[] = linea.split(" ");
				Coordenada nuevaCoord = new Coordenada(Double.parseDouble(arrAux[0]),Double.parseDouble(arrAux[1]));
				
				coords.add(nuevaCoord);
			}
			
			scanner.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Revise el formato/contenido del archivo.\n\n"+e.toString(), "** ERROR **", 0);
		}
		
		return coords;
	}
	
	public static Grafo leerGrafoDesdeTxt(String ruta) {
		Grafo grafo = new Grafo();
		
		try {
			FileInputStream fis = new FileInputStream(ruta);
			Scanner scanner = new Scanner(fis);
			
			while (scanner.hasNext()) {
				String[] arrAux = scanner.nextLine().split(" ");
				Vertice vertice=new Vertice();
				try {
					vertice.stringAVertice(arrAux);	
					grafo.agregarVertice(vertice);	
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Se encontr� un error al leer los datos del v�rtice.\nRevise el archivo de datos.\n\n"+e.toString(), "** ERROR **", 0);
				}
				
			}
			
			scanner.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Revise el formato/contenido del archivo.\n\n", "** ERROR **", 0);
			e.printStackTrace();
		}
		
		return grafo;
	}
	
	public static String devuelveRutaArchivoConFileChooser(EnumFileChooser condicion) {
		File directorioProyecto = new File(System.getProperty("user.dir"));
		JFileChooser selectorRuta = new JFileChooser();

		FileNameExtensionFilter filtroTxt = new FileNameExtensionFilter("Archivos de texto plano", "txt", "text");
		selectorRuta.setFileFilter(filtroTxt);
		selectorRuta.setCurrentDirectory(directorioProyecto);
		int returnValue = 0;
		switch (condicion) {
			case ABRIR:
				returnValue = selectorRuta.showOpenDialog(null);
				break;
			case GUARDAR:
				returnValue = selectorRuta.showSaveDialog(null);
				break;
		}		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File selectedFile = selectorRuta.getSelectedFile();
			return selectedFile.getAbsolutePath();
		}		
		return "";
	}

	
	
	
}
