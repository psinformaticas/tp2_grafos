package controlador;

public class FuncionesNumeros {

	//FUNCIONES PARA DECIMALES
	public static Double acortarDecimales(Double numero, Integer numeroDecimales) {
        return Math.round(numero * Math.pow(10, numeroDecimales)) / Math.pow(10, numeroDecimales);
    }

}
