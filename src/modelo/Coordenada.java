package modelo;

public class Coordenada {

	private double latitud;
	private double longitud;
	
	public Coordenada(double lat, double lon) {
		latitud = lat;
		longitud = lon;
	}
	
	public Coordenada() {
		latitud = 0.0;
		longitud = 0.0;
	}

	@Override
	public String toString() {
		return Double.toString(latitud) + " " + Double.toString(longitud);
	}
	
	//GETTERS Y SETTERS
	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {
			Coordenada coord = (Coordenada) obj;
			if (longitud!=coord.getLongitud() || latitud!=coord.getLatitud()) {
				return false;
			}
			
		}
		
		return true;
	}
	
}
