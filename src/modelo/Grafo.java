package modelo;

import java.util.ArrayList;

import controlador.FuncionesNumeros;
import controlador.FuncionesArchivos;

public class Grafo 
{
	private ArrayList<Vertice> arrayTodosLosVertices;
	private int cantVertices; 
	private int cantClusters;
	
	public Grafo() {
		arrayTodosLosVertices = new ArrayList<>();
		cantVertices = 0;
		cantClusters = 0;
	}

	
	public Grafo(ArrayList<Vertice> vertices) {
		
		for (Vertice vert: vertices) {
			arrayTodosLosVertices.add(vert);
		}
		
		cantVertices = vertices.size();
	}
	
	public void actualizaVecinosEnVerticesRecibeVerticeNuevo(Vertice vertNuevo) {
		//AGREGA VERTICES VECINOS - GRAFO COMPLETO
		for (Vertice vertice: arrayTodosLosVertices) {
			if (vertice.getIdVertice() != vertNuevo.getIdVertice()) {
				vertNuevo.addIdVecino(vertice.getIdVertice());
				vertice.addIdVecino(vertNuevo.getIdVertice());
			}
		}
	}
	
	public void establecerComoVecinos(Vertice vert1, Vertice vert2) {
		//AGREGA VERTICES VECINOS - GRAFO COMPLETO
		vert1.addIdVecino(vert2.getIdVertice());
		vert2.addIdVecino(vert1.getIdVertice());
	}
	
	public void vaciarListasVecinos() {
		cantVertices=0;
		for (Vertice vertice: arrayTodosLosVertices) {
			vertice.getIdsVerticesVecinos().clear();
		}
	}
	
	public void agregarVertice(Vertice vertice) {
		arrayTodosLosVertices.add(vertice);
		cantVertices++;
		vertice.setIdVertice(cantVertices);
	}
	
	public void cargarCoordenadasAGrafoDesdeTxt(String ruta) {
		ArrayList<Coordenada> coords = FuncionesArchivos.leerCoordDesdeTxt(ruta);
		Vertice verticeNuevo;

		for (Coordenada coordenada: coords) {
			verticeNuevo = new Vertice(cantVertices+1, coordenada);
			
			//AGREGA VERTICES VECINOS - GRAFO COMPLETO
			actualizaVecinosEnVerticesRecibeVerticeNuevo(verticeNuevo);
			
			arrayTodosLosVertices.add(verticeNuevo);
			cantVertices++;
		}
	}
	public void guardarGrafoATxt(String ruta) {
		FuncionesArchivos.guardarGrafoATxt(arrayTodosLosVertices,ruta);	
	}
	public void imprimir() {
		System.out.println(this);
	}
	
	
	@Override
	public String toString() {
		String ret = "";
		for (Vertice vert: arrayTodosLosVertices) {
			ret += vert.toString();
		}
		
		return ret;
	}
	
	public ArrayList<Vertice> getVertices() {
		return arrayTodosLosVertices;
	}
	
	public Vertice getVerticePorId(int id) {
		Vertice v = new Vertice();
		for (Vertice vert: arrayTodosLosVertices) {
			if (vert.getIdVertice() == id) {
				v = vert;
			}
		}
		return v;
	}
	
	public double[][] getMatrizDePesos() {
		double[][] matrizDePesos = new double[cantVertices][cantVertices];
		
		for (int i = 0; i<arrayTodosLosVertices.size(); i++) {
			for (int j = 0; j<arrayTodosLosVertices.size(); j++) {
				matrizDePesos[i][j] = getVerticePorId(i+1).getDistanciaAOtroVertice(getVerticePorId(j+1));
			}
		}
		
		return matrizDePesos;
	}

	public void imprimirMatrizDePesos() {
		double[][] matrizDePesos = getMatrizDePesos();
		//CABECERAS
		System.out.print("\t");
		for (int i = 0; i<matrizDePesos.length; i++) {
			System.out.print("V: "+(i+1)+"\t");
		}
		System.out.print("\n");
		
		
		//DATOS
		for (int i = 0; i<matrizDePesos.length; i++) {
			System.out.print("V: "+(i+1)+"\t");
			for (int j = 0; j<matrizDePesos[0].length; j++) {
				System.out.print(matrizDePesos[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}
	
	public int getCantVertices(){
		return cantVertices;
	}
	
	//FUNCIONES PARA CLUSTERIZACION
	public double getPesoPromedioAristas() {
		double sumaDePesos = 0.0;
		double pesoPromedio = 0.0;
		int cantPesos = 0;
		
		for (Vertice vertice: arrayTodosLosVertices) {
			for (Integer idVertVecino: vertice.getIdsVerticesVecinos()) {
				
				sumaDePesos += vertice.getDistanciaAOtroVertice(getVerticePorId(idVertVecino));
				cantPesos++;
			}
		}
		
		
		pesoPromedio = sumaDePesos/cantPesos;
		
		return FuncionesNumeros.acortarDecimales(pesoPromedio,2);
	}
	
	public double getDesviacionEstandarAristas() {
		double sumaDePesos = 0.0;
		int cantPesos = 0;
		ArrayList<Double> pesos = new ArrayList<>();
		
		for (Vertice vertice: arrayTodosLosVertices) {
			for (Integer idVertVecino: vertice.getIdsVerticesVecinos()) {
				sumaDePesos += vertice.getDistanciaAOtroVertice(getVerticePorId(idVertVecino));
				pesos.add(vertice.getDistanciaAOtroVertice(getVerticePorId(idVertVecino)));
				cantPesos++;
			}
		}
		
		double promedio = sumaDePesos / cantPesos;
		double rango;
		double varianza = 0.0;
		
		for(Double peso: pesos){
		   rango = Math.pow(peso - promedio, 2f);
		   varianza = varianza + rango;
		}
		
		varianza = varianza / cantPesos;
		double desviacion = Math.sqrt(varianza);
		
		return FuncionesNumeros.acortarDecimales(desviacion,2);
	}
	
	
	public void eliminarAristasPorCondicion(CondicionesParaCluster condicion) {
		
		int aristasIniciales = getCantAristas();
		int aristasfinales = 0;
		
		double pesoProm = getPesoPromedioAristas();
		double desvioStd = getDesviacionEstandarAristas();
		ArrayList<Vertice> verticesAux = new ArrayList<>();
		//CARGA TODOS LOS VERTICES AL ARRAY AUXILIAR
		for (Vertice v: arrayTodosLosVertices) {
			Vertice nVertice = new Vertice();
			nVertice.setCoord(v.getCoord());
			nVertice.setIdVertice(v.getIdVertice());
			verticesAux.add(nVertice);
		}
		
		//ELIMINA VECINOS DE TODOS LOS VERTICES
		for (Vertice v: verticesAux) {
			v.getIdsVerticesVecinos().clear();
		}
		
		//COMPRUEBA SI PESOS SON MAYORES AL PROMEDIO Y NO LOS AGREGA
		for (int i = 0; i<arrayTodosLosVertices.size(); i++) {
			for (Integer idVertVecino: arrayTodosLosVertices.get(i).getIdsVerticesVecinos()) {
				switch (condicion)
			    {
			      case DESVIACIONSTD:
			    	  if (arrayTodosLosVertices.get(i).getDistanciaAOtroVertice(getVerticePorId(idVertVecino)) < (pesoProm+desvioStd)) {
							verticesAux.get(i).addIdVecino(idVertVecino);
			    	  }
			      case PROMEDIO:
			    	  if (arrayTodosLosVertices.get(i).getDistanciaAOtroVertice(getVerticePorId(idVertVecino)) < pesoProm) {
							verticesAux.get(i).addIdVecino(idVertVecino);
			    	  }
			    	  break;
			    }
				
			}
		}
		
		arrayTodosLosVertices = verticesAux;
		
		aristasfinales = getCantAristas();
		cantClusters = (aristasIniciales - aristasfinales)+1;
		
	}
	
	public int getCantAristas() {
		int cantAristas = 0;
		for (Vertice vertice: arrayTodosLosVertices) {
			for(Integer idVerticeVecino: vertice.getIdsVerticesVecinos()) {
				cantAristas++;
			}
		}
		
		cantAristas = cantAristas/2;
		return cantAristas;
	}
	
	public int getCantClusters() {
		return cantClusters;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {

			Grafo grafo2= (Grafo) obj;
			if(cantVertices!=grafo2.cantVertices){
				return false;
			}
			for (int i = 0; i < arrayTodosLosVertices.size(); i++) {
				if ( !arrayTodosLosVertices.get(i).equals(grafo2.getVertices().get(i)) || cantClusters!=grafo2.getCantClusters()) {
					return false;
				}
			}
		
		}
		
		return true;
	}
}
