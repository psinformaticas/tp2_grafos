package modelo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GrafoTest {
//	Grafo grafo;
//	Grafo grafo2;
	
	Grafo grafo=new Grafo();
	Grafo grafo2=new Grafo();
	
	Coordenada c1=new Coordenada(-34.52133782929332,-58.70068073272705);
	Coordenada c2=new Coordenada(-34.520772089706036,-58.702311515808105);
	
	Vertice v1= new Vertice(1,c1);
	Vertice v2= new Vertice(2,c2);
	
	@Before
	public void incializar(){
		
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo2.agregarVertice(v1);
		
	}
	@Test
	public void actualizaVecinosEnVerticesRecibeVerticeNuevoTest() {
		//En este metodo se realiza tambien el Test de ""establecerComoVecinos"". 
		grafo.establecerComoVecinos(v1, v2);
		grafo2.agregarVertice(v2);
		grafo2.actualizaVecinosEnVerticesRecibeVerticeNuevo(v2);
		assertTrue(grafo.equals(grafo2));
	}
	@Test
	public void vaciarListasVecinosTest(){
		grafo.vaciarListasVecinos();
		assertEquals(grafo.getCantVertices(),0);
	}

}
