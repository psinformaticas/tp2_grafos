package modelo;

import controlador.FuncionesArchivos;
import controlador.AlgoritmoDePrim;


public class Mapa {

	private Grafo grafoOriginal;
	
	private Grafo grafoAGM;
		
	public Mapa() {
		grafoOriginal = new Grafo();
		grafoAGM = new Grafo();
	}
	
	public void cargarCoordsDesdeTxtAGrafo(String ruta) {
		grafoOriginal.cargarCoordenadasAGrafoDesdeTxt(ruta);	
	}
	
	public void guardarGrafoATxt(String ruta) {
		grafoAGM.guardarGrafoATxt(ruta);	
	}
	
	public void cargarGrafoAGM(String ruta){
		grafoAGM=FuncionesArchivos.leerGrafoDesdeTxt(ruta);
	}
	
	
	public int getCantCoordenadasEnGrafo(Grafo grafo) {
		return grafo.getVertices().size();
	}
	
	public Grafo getGrafoOriginal() {
		return grafoOriginal;
	}
	
	public Grafo getGrafoAGM() {
		return grafoAGM;
	}
	
	public void setGrafoOriginal(Grafo grafo) {
		grafoOriginal = grafo;
	}
	
	public void setGrafoAGM(Grafo grafo) {
		grafoAGM = grafo;
	}
	
	public void generarAGM() {
		grafoAGM = AlgoritmoDePrim.generarAGM(getGrafoOriginal());
	}
	
}
