package modelo;


import java.util.HashSet;
import java.util.Set;
public class Vertice {


	private int idVertice; 
	private Coordenada coordenada;
	private Set<Integer> idsVerticesVecinos;
	
	public Vertice() {
		coordenada = new Coordenada();
		idVertice = 0;
		idsVerticesVecinos = new HashSet<>();
	}
	
	public Vertice(int id, Coordenada coord) {
		coordenada = coord;
		idVertice = id;
		idsVerticesVecinos = new HashSet<>();
	}

	//FUNCIONES PROPIAS
	
	@Override
	public String toString() {
		String vecinos = "";
		for (Integer idVecino: idsVerticesVecinos) {
			vecinos += Integer.toString(idVecino)+"-";
			}
		String ret = getIdVertice()+ " "+getCoord().toString()+" "+vecinos;
		
		return ret;
	}	
	
	public void addIdVecino(int id) {
		idsVerticesVecinos.add(id);
	}
	
	public double getDistanciaAOtroVertice(Vertice otroVertice) {
		    final int R = 6371; //Radio de la tierra

		    double distanciaLatitud = Math.toRadians(otroVertice.getCoord().getLatitud() - coordenada.getLatitud());
		    double distanciaLongitud = Math.toRadians(otroVertice.getCoord().getLongitud() - coordenada.getLongitud());
		    double a = Math.sin(distanciaLatitud / 2) * Math.sin(distanciaLatitud / 2)
		            + Math.cos(Math.toRadians(coordenada.getLatitud())) * Math.cos(Math.toRadians(otroVertice.getCoord().getLatitud()))
		            * Math.sin(distanciaLongitud / 2) * Math.sin(distanciaLongitud / 2);
		    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		    double distancia = R * c * 1000; // convierte a metros

		    distancia = Math.pow(distancia, 2);

		    return Math.sqrt(distancia);
		
	}
	
	public void stringAVertice(String[] string){
		setIdVertice(Integer.parseInt(string[0]));
		Coordenada nuevaCoord = new Coordenada(Double.parseDouble(string[1]),Double.parseDouble(string[2]));
		setCoord(nuevaCoord);
		String arrAux[];
		if (string.length>3) {
			arrAux = string[3].split("-");	
			for (String s: arrAux) {
				addIdVecino(Integer.parseInt(s));
			}
		}
		
	}
	
	public void setIdsVerticesVecinos(Set<Integer> idsVerticesVecinos) {
		this.idsVerticesVecinos = idsVerticesVecinos;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {
			Vertice  vertice2 = (Vertice) obj;
			if( idVertice!=vertice2.getIdVertice() || idsVerticesVecinos.size()!=vertice2.getIdsVerticesVecinos().size()){
				
				return false;
			}
			if(!idsVerticesVecinos.containsAll(vertice2.getIdsVerticesVecinos()) || !coordenada.equals(vertice2.getCoord())){
				return false;
			}		
		}
		
		return true;
	}
	
	//GETTERS & SETTERS GENÉRICOS
	
	public int getIdVertice() {
		return idVertice;
	}

	public void setIdVertice(int idVertice) {
		this.idVertice = idVertice;
	}

	public Coordenada getCoord() {
		return coordenada;
	}

	public void setCoord(Coordenada coordenada) {
		this.coordenada = coordenada;
	}

	public Set<Integer> getIdsVerticesVecinos() {
		return idsVerticesVecinos;
	}
	
	
	
}
