package modelo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VerticeTest {
	Vertice vertice;
	//TXT VERTICE
	String lineaInput = "1 -34.52133782929332 -58.70068073272705 2-68-";
	
	@Before
	public void incializar(){
		Coordenada c1=new Coordenada(-34.52133782929332, -58.70068073272705);
		vertice=new Vertice(1, c1);
		vertice.addIdVecino(2);
		vertice.addIdVecino(68);
	}
	
	@Test
	public void stringAVerticeTest() {
		Vertice verticeTest = new Vertice();
		verticeTest.stringAVertice(lineaInput.split(" "));
		
		assertTrue(vertice.equals(verticeTest));
	}

}
