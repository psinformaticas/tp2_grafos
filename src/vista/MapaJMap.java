package vista;

import java.awt.Color;
import java.util.ArrayList;

import modelo.Coordenada;
import modelo.Grafo;
import modelo.Mapa;
import modelo.Vertice;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.OsmMercator;

import controlador.FuncionesNumeros;
import controlador.FuncionesArchivos;
import controlador.AlgoritmoDePrim;


public class MapaJMap extends JMapViewer{

	private Mapa mapa;
	
	private MapPolygonImpl aristaGrafoOriginal;
	public ArrayList<MapPolygonImpl> aristasGrafoOriginal;
	
	private MapPolygonImpl aristaAGM;
	public ArrayList<MapPolygonImpl> aristasGrafoAGM;
	
	private ArrayList<Coordinate> coordinatesEnMapa;
	
	
	public MapaJMap(Mapa mapaBase) {
		mapa = mapaBase;
		coordinatesEnMapa = new ArrayList<>();
	}
	
	public void cargarCoordsDesdeTxtAGrafo(String ruta) {
		mapa.getGrafoOriginal().cargarCoordenadasAGrafoDesdeTxt(ruta);	
	}
	
	public void guardarGrafoATxt(String ruta) {
		mapa.getGrafoAGM().guardarGrafoATxt(ruta);	
	}
	
	public void cargarGrafoAGM(String ruta){
		mapa.setGrafoAGM(FuncionesArchivos.leerGrafoDesdeTxt(ruta));
	}
	
	public void marcarCoordEnMapaDesdeGrafo(Grafo grafo) {
		for (Vertice vertice: grafo.getVertices()) {
			Coordinate coord = new Coordinate(vertice.getCoord().getLatitud(), vertice.getCoord().getLongitud());
			coordinatesEnMapa.add(coord);
			
			this.addMapMarker(new MapMarkerDot(Integer.toString(vertice.getIdVertice()), coord));
		}
	}
	
	public void dibujarAristasGrafo(Grafo grafo, boolean esAGM) {
		if (esAGM) {
			aristasGrafoAGM = new ArrayList<MapPolygonImpl>();
		} else {
			aristasGrafoOriginal = new ArrayList<MapPolygonImpl>();
		}
		
		for (Vertice vertice: grafo.getVertices()) {
			for (Integer idVertVecino: vertice.getIdsVerticesVecinos())
			{
				if (esAGM) {
					aristaAGM = new MapPolygonImpl(coordenadaACoordinateJMap(vertice.getCoord()), coordenadaACoordinateJMap(mapa.getGrafoAGM().getVerticePorId(idVertVecino).getCoord()), coordenadaACoordinateJMap(vertice.getCoord()));
					aristasGrafoAGM.add(aristaAGM);
					aristaAGM.setColor(Color.RED);
					aristaAGM.setStroke(aristaAGM.getStroke());
					this.addMapPolygon(aristaAGM);
					
				} else {
					aristaGrafoOriginal = new MapPolygonImpl(coordenadaACoordinateJMap(vertice.getCoord()), coordenadaACoordinateJMap(mapa.getGrafoOriginal().getVerticePorId(idVertVecino).getCoord()), coordenadaACoordinateJMap(vertice.getCoord()));
					aristasGrafoOriginal.add(aristaGrafoOriginal);
					aristaGrafoOriginal.setColor(Color.BLUE);
					this.addMapPolygon(aristaGrafoOriginal);
				}
				
			}
		}
	}
	
	public Coordinate coordenadaACoordinateJMap(Coordenada coordenada) {
		return new Coordinate(coordenada.getLatitud(), coordenada.getLongitud());
	}
	
	public double distanciaEnMetrosEntre2Vertices(Vertice vert1, Vertice vert2) {
		OsmMercator osm = new OsmMercator();
		return FuncionesNumeros.acortarDecimales(osm.getDistance(vert1.getCoord().getLatitud(), vert1.getCoord().getLongitud(), vert2.getCoord().getLatitud(), vert2.getCoord().getLongitud()),2);
	}
	public void limpiarMapa() {
		mapa.setGrafoOriginal(new Grafo());
		mapa.setGrafoAGM(new Grafo());
		coordinatesEnMapa.clear();
		this.removeAllMapMarkers();
		this.removeAllMapPolygons();
		Coordinate coord = new Coordinate(-34.534067, -58.703758);
		this.setDisplayPosition(coord, 14);
	}

	public void ocultarCoordenadasEnMapa() {
		this.removeAllMapMarkers();
	}
	
	public void ocultarAristasEnMapa() {
		this.removeAllMapPolygons();
	}
	
	public int getCantCoordenadasEnGrafo(Grafo grafo) {
		return grafo.getVertices().size();
	}
	
	public Grafo getGrafoOriginal() {
		return mapa.getGrafoOriginal();
	}
	
	public Grafo getGrafoAGM() {
		return mapa.getGrafoAGM();
	}
	
	public void setGrafoAGM(Grafo grafo) {
		mapa.setGrafoAGM(grafo);
	}
	
	public void generarAGM() {
		mapa.setGrafoAGM(AlgoritmoDePrim.generarAGM(getGrafoOriginal()));
	}
	
}
