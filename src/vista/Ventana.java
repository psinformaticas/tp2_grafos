package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import modelo.Mapa;
import modelo.CondicionesParaCluster;
import modelo.EnumFileChooser;

import org.openstreetmap.gui.jmapviewer.Coordinate;



import controlador.FuncionesArchivos;




import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import java.awt.Color;

import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JLabel;

import java.awt.Font;


public class Ventana {

	private JFrame frame;
	private Mapa mapaBase;
	private MapaJMap mapaJMap;
	private Coordinate coord;
	private JLabel labelInfo;
	
	JLabel lbEstado1;
	JLabel lbEstado2;
	
	JButton btnGenerarAGM;
	JButton btnDibujarAristasAGM;
	JButton btnClusterPromedio;
	JButton btnClusterDesvio;
	JButton btnReiniciarMapa;
	JButton btnCargarCoordTxt;
	JButton btnDibujarCoordenadas;
	JButton btnDibujarAristasOrig;
	JButton btnGuardarEnTxt;
	JButton btnCargarGrafoAGM;
	JButton btnOcultarCoordEnMapa;
	JButton btnOcultarAristas;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 1120, 709);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Trabajo Pr�ctivo N� 2  -  Castillo / Vrhovski  -  UNGS  -  Programaci�n 3  -  A�o 2019");
		
		mapaBase = new Mapa();
		mapaJMap = new MapaJMap(mapaBase);
		mapaJMap.setBounds(10, 20, 660, 590);
		
		coord = new Coordinate(-34.534067, -58.703758);
		mapaJMap.setDisplayPosition(coord, 14);
		
		
		JPanel panelMapa = new JPanel();
		panelMapa.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Mapa", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelMapa.setBounds(12, 13, 680, 620);
		frame.getContentPane().add(panelMapa);
		panelMapa.setLayout(null);
		panelMapa.add(mapaJMap);
		
		
		
		
		JPanel panelBotones = new JPanel();
		panelBotones.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Panel de Acciones", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelBotones.setBounds(704, 13, 398, 620);
		frame.getContentPane().add(panelBotones);
		panelBotones.setLayout(null);
		
		JPanel panelAGM = new JPanel();
		panelAGM.setBackground(Color.WHITE);
		panelAGM.setBounds(12, 296, 374, 212);
		panelBotones.add(panelAGM);
		panelAGM.setLayout(null);
		
		JPanel barraPanelAGM = new JPanel();
		barraPanelAGM.setBackground(Color.GRAY);
		barraPanelAGM.setBounds(0, 0, 374, 35);
		panelAGM.add(barraPanelAGM);
		barraPanelAGM.setLayout(null);
		
		JPanel decoPanelAGM = new JPanel();
		decoPanelAGM.setBackground(new Color(50, 205, 50));
		decoPanelAGM.setBounds(0, 0, 16, 35);
		barraPanelAGM.add(decoPanelAGM);
		
		JLabel lblNewLabel_1 = new JLabel("Acciones Grafo AGM");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(26, 0, 336, 35);
		barraPanelAGM.add(lblNewLabel_1);
		
		btnGenerarAGM = new JButton("<html><center>Generar<br>AGM</center></html>");
		btnGenerarAGM.setEnabled(false);
		btnGenerarAGM.setBounds(20, 48, 135, 41);
		panelAGM.add(btnGenerarAGM);
		
		btnDibujarAristasAGM = new JButton("<html><center>Dibujar<br>aristas</center></html>");
		btnDibujarAristasAGM.setEnabled(false);
		btnDibujarAristasAGM.setBounds(20, 102, 135, 41);
		panelAGM.add(btnDibujarAristasAGM);
		
		btnClusterPromedio = new JButton("<html><center>Generar Clusters<br>por Promedio</center></html>");
		btnClusterPromedio.setEnabled(false);
		btnClusterPromedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mapaJMap.getGrafoAGM().eliminarAristasPorCondicion(CondicionesParaCluster.PROMEDIO);
				lbEstado1.setText("Peso promedio de aristas: "+mapaJMap.getGrafoAGM().getPesoPromedioAristas());
				lbEstado2.setText("Cant. de clusters: "+mapaJMap.getGrafoAGM().getCantClusters());
				btnClusterDesvio.setEnabled(false);
				btnClusterPromedio.setEnabled(false);
			}
		});
		btnClusterPromedio.setBounds(224, 48, 135, 41);
		panelAGM.add(btnClusterPromedio);
		
		btnClusterDesvio = new JButton("<html><center>Generar Clusters<br>por Desv�o Std.</center></html>");
		btnClusterDesvio.setEnabled(false);
		btnClusterDesvio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mapaJMap.getGrafoAGM().eliminarAristasPorCondicion(CondicionesParaCluster.DESVIACIONSTD);
				lbEstado1.setText("P. Prom.: "+mapaJMap.getGrafoAGM().getPesoPromedioAristas()+ "       Desv. std.: "+mapaJMap.getGrafoAGM().getDesviacionEstandarAristas());
				lbEstado2.setText("Cant. de clusters: "+mapaJMap.getGrafoAGM().getCantClusters());
				btnClusterDesvio.setEnabled(false);
				btnClusterPromedio.setEnabled(false);
			}
		});
		btnClusterDesvio.setBounds(224, 102, 135, 41);
		panelAGM.add(btnClusterDesvio);
		
		btnCargarGrafoAGM = new JButton("<html><center>Cargar Grafo<br>AGM desde TXT</center></html>");
		btnCargarGrafoAGM.setBounds(224, 156, 135, 41);
		panelAGM.add(btnCargarGrafoAGM);
		
		btnGuardarEnTxt = new JButton("<html><center>Guardar AGM<br>En TXT</center></html>");
		btnGuardarEnTxt.setBounds(20, 156, 135, 41);
		panelAGM.add(btnGuardarEnTxt);
		btnGuardarEnTxt.setEnabled(false);
		btnGuardarEnTxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String rutaTxt = FuncionesArchivos.devuelveRutaArchivoConFileChooser(EnumFileChooser.GUARDAR);
				mapaJMap.guardarGrafoATxt(rutaTxt);
			}
		});
		btnCargarGrafoAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String rutaTxt = FuncionesArchivos.devuelveRutaArchivoConFileChooser(EnumFileChooser.ABRIR);
				mapaJMap.cargarGrafoAGM(rutaTxt);
				setLabelInfo("CARGADO: "+rutaTxt+"      ["+mapaJMap.getCantCoordenadasEnGrafo(mapaJMap.getGrafoAGM())+" COORDENADAS]");
				//lbEstado1.setText("Grafo cargado, se han cargado coordenadas.");
				activarBotonesConGrafoCargado(true);
				
				mapaJMap.marcarCoordEnMapaDesdeGrafo(mapaJMap.getGrafoAGM());
				mapaJMap.dibujarAristasGrafo(mapaJMap.getGrafoAGM(), true);
				btnCargarGrafoAGM.setEnabled(false);
			}
		});
		btnDibujarAristasAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.ocultarAristasEnMapa();
				mapaJMap.dibujarAristasGrafo(mapaJMap.getGrafoAGM(), true);
				btnGuardarEnTxt.setEnabled(true);
			}
		});
		btnGenerarAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.generarAGM();
				activarBotonesAGM(true);
				lbEstado2.setText("Se ha generado el AGM para el grafo.");
			}
		});
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(12, 185, 374, 101);
		panelBotones.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBackground(Color.GRAY);
		panel_2.setBounds(0, 0, 374, 35);
		panel_1.add(panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(50, 205, 50));
		panel_3.setBounds(0, 0, 16, 35);
		panel_2.add(panel_3);
		
		JLabel lblAccionesGrafoOriginal = new JLabel("Acciones Grafo Completo");
		lblAccionesGrafoOriginal.setForeground(Color.WHITE);
		lblAccionesGrafoOriginal.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblAccionesGrafoOriginal.setBounds(26, 0, 336, 35);
		panel_2.add(lblAccionesGrafoOriginal);
		
		btnDibujarAristasOrig = new JButton("<html><center>Dibujar<br>aristas</center></html>");
		btnDibujarAristasOrig.setEnabled(false);
		btnDibujarAristasOrig.setBounds(20, 48, 135, 41);
		panel_1.add(btnDibujarAristasOrig);
		
		btnOcultarAristas = new JButton("<html><center>Ocultar<br>aristas</center></html>");
		btnOcultarAristas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.ocultarAristasEnMapa();
			}
		});
		btnOcultarAristas.setEnabled(false);
		btnOcultarAristas.setBounds(224, 48, 135, 41);
		panel_1.add(btnOcultarAristas);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBackground(Color.WHITE);
		panel_4.setBounds(12, 27, 374, 150);
		panelBotones.add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBackground(Color.GRAY);
		panel_5.setBounds(0, 0, 374, 35);
		panel_4.add(panel_5);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(new Color(50, 205, 50));
		panel_6.setBounds(0, 0, 16, 35);
		panel_5.add(panel_6);
		
		JLabel lblAccionesGenerales = new JLabel("Acciones Generales");
		lblAccionesGenerales.setForeground(Color.WHITE);
		lblAccionesGenerales.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblAccionesGenerales.setBounds(26, 0, 336, 35);
		panel_5.add(lblAccionesGenerales);
		
		btnCargarCoordTxt = new JButton("<html><center>Cargar Coord.<br>desde TXT</center></html>");
		btnCargarCoordTxt.setBounds(20, 48, 135, 41);
		panel_4.add(btnCargarCoordTxt);
		
		btnDibujarCoordenadas = new JButton("<html><center>Dibujar Coord.<br>cargadas</center></html>");
		btnDibujarCoordenadas.setEnabled(false);
		btnDibujarCoordenadas.setBounds(20, 98, 135, 41);
		panel_4.add(btnDibujarCoordenadas);
		
		btnReiniciarMapa = new JButton("<html><center>Reiniciar<br>mapa</center></html>");
		btnReiniciarMapa.setBounds(224, 48, 135, 41);
		panel_4.add(btnReiniciarMapa);
		
		btnOcultarCoordEnMapa = new JButton("<html><center>Ocultar Coord.<br>cargadas</center></html>");
		btnOcultarCoordEnMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.ocultarCoordenadasEnMapa();
			}
		});
		btnOcultarCoordEnMapa.setEnabled(false);
		btnOcultarCoordEnMapa.setBounds(224, 98, 135, 41);
		panel_4.add(btnOcultarCoordEnMapa);
		btnReiniciarMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.limpiarMapa();
				setLabelInfo("Se ha reiniciado el mapa");
				lbEstado1.setText("No se ha cargado ning�n grafo / coordenada.");
				lbEstado2.setText("");
				reiniciarBotones();
			}
		});
		
		JPanel panel_7 = new JPanel();
		panel_7.setBounds(12, 531, 374, 76);
		panelBotones.add(panel_7);
		panel_7.setLayout(null);
		panel_7.setBackground(Color.WHITE);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBackground(Color.GRAY);
		panel_8.setBounds(0, 0, 374, 22);
		panel_7.add(panel_8);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(new Color(50, 205, 50));
		panel_9.setBounds(0, 0, 16, 22);
		panel_8.add(panel_9);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setForeground(Color.WHITE);
		lblEstado.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEstado.setBounds(26, 0, 336, 22);
		panel_8.add(lblEstado);
		
		lbEstado1 = new JLabel("No se ha cargado ning\u00FAn grafo / coordenada.");
		lbEstado1.setBounds(10, 29, 352, 16);
		panel_7.add(lbEstado1);
		
		lbEstado2 = new JLabel("");
		lbEstado2.setBounds(10, 47, 352, 16);
		panel_7.add(lbEstado2);
		btnDibujarCoordenadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.marcarCoordEnMapaDesdeGrafo(mapaJMap.getGrafoOriginal());
			}
		});
		btnCargarCoordTxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String rutaCoordTxt = FuncionesArchivos.devuelveRutaArchivoConFileChooser(EnumFileChooser.ABRIR);
				mapaJMap.cargarCoordsDesdeTxtAGrafo(rutaCoordTxt);
				setLabelInfo("CARGADO: "+rutaCoordTxt+"      ["+mapaJMap.getCantCoordenadasEnGrafo(mapaJMap.getGrafoOriginal())+" COORDENADAS]");
				lbEstado1.setText("Grafo cargado, se han cargado coordenadas.");
				activarBotonesConGrafoCargado(true);
			}
		});
		btnDibujarAristasOrig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mapaJMap.ocultarAristasEnMapa();
				mapaJMap.dibujarAristasGrafo(mapaJMap.getGrafoOriginal(), false);
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 646, 1114, 28);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		labelInfo = new JLabel(" ");
		labelInfo.setBounds(12, 0, 653, 28);
		panel.add(labelInfo);

	}

	private void activarBotonesConGrafoCargado(boolean activo) {
		btnGenerarAGM.setEnabled(activo);
		btnReiniciarMapa.setEnabled(activo);
		btnGuardarEnTxt.setEnabled(activo);
		btnCargarCoordTxt.setEnabled(!activo);
		btnDibujarCoordenadas.setEnabled(activo);
		btnOcultarCoordEnMapa.setEnabled(activo);
		btnDibujarAristasOrig.setEnabled(activo);
		btnOcultarAristas.setEnabled(activo);
		
		btnCargarGrafoAGM.setEnabled(!activo);
		btnDibujarAristasAGM.setEnabled(!activo);
		btnClusterPromedio.setEnabled(!activo);
		btnClusterDesvio.setEnabled(!activo);
	}
	
	private void activarBotonesAGM(boolean activo) {
		btnDibujarAristasAGM.setEnabled(activo);
		btnClusterPromedio.setEnabled(activo);
		btnClusterDesvio.setEnabled(activo);
	}
	
	private void reiniciarBotones() {
		btnGenerarAGM.setEnabled(false);
		btnCargarCoordTxt.setEnabled(true);
		btnDibujarCoordenadas.setEnabled(false);
		btnDibujarAristasOrig.setEnabled(false);
		btnGuardarEnTxt.setEnabled(false);
		btnCargarGrafoAGM.setEnabled(true);
		btnDibujarAristasAGM.setEnabled(false);
		btnClusterPromedio.setEnabled(false);
		btnClusterDesvio.setEnabled(false);
		btnOcultarCoordEnMapa.setEnabled(false);
		btnOcultarAristas.setEnabled(false);
	}
	
	
	private void setLabelInfo(String info) {
		labelInfo.setText(info);
	}
}
